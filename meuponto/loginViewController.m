//
//  loginViewController.m
//  meuponto
//
//  Created by Marcel Medeiros on 11/10/14.
//  Copyright (c) 2014 Fourop. All rights reserved.
//

#import "loginViewController.h"
#import "loginShare.h"
#import "AFNetworking.h"
#import "DCKeyValueObjectMapping.h"


@interface loginViewController ()
{
    NSArray *dataLogin;
    BOOL *valid;
    
}
@end

@implementation loginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)btnLogin:(id)sender {

    [_activity startAnimating];
    
    NSLog(@"User name | %@", _txtUsername.text);
    NSLog(@"Password | %@", _txtPassword.text);
    
    if ([ [_txtUsername text] length] == 0 || [[_txtPassword text] length] == 0 ){
        
        // Login invalido
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                        message:@"Email ou senha invalido!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [_activity stopAnimating];
        
        [alert show];
        
        
        return;
        
        
    }else{

    //___________Login____________//
    
    NSString *URL_JSON = @"https://4op.adamaseps.com.br/mobile/Gestao/ServicoFluxoOperacional.svc/VerificarLogin/";
    NSString *username = _txtUsername.text;
    NSString *password = _txtPassword.text;
        
    
    NSString *SEND_JSON = [URL_JSON stringByAppendingString:username];
    SEND_JSON = [SEND_JSON stringByAppendingString:@"/"];
    SEND_JSON = [SEND_JSON stringByAppendingString:password];
    
    NSLog(@"%@",SEND_JSON);
        
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:SEND_JSON  parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        dataLogin =[responseObject objectForKey:@"VerificarLoginResult"];
        
        NSString *status = [dataLogin valueForKeyPath:@"Status"];
        
        if ([status isEqualToString:@"OK"]) {
            
            NSString *URL_MEUPONTO = [NSString stringWithFormat:@"http://master.fourop.com.br/Fourop/get.jsp?email=%@&pwd=%@",_txtUsername.text,_txtPassword.text];
            
            
            loginShare *login = [loginShare sharedInstance];
            login.loginLogin = _txtUsername.text;
            login.url = URL_MEUPONTO;
            
            UIViewController *vc;
            UIStoryboard * mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"meuPonto"];
            vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:vc animated:YES completion:NULL];
            
            
        }else{
            // Login invalido
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!!"
                                                            message:@"Email ou senha invalido"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
    }];
    
    }
    [_activity stopAnimating];
    
}


#pragma mark - Hide keywords
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_txtUsername resignFirstResponder];
    [_txtPassword resignFirstResponder];
    
    [self becomeFirstResponder];
    
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

- (IBAction)btnNewAccount:(id)sender {
    
    // Login invalido
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!!!"
                                                    message:@"Entre em contato com: contato@fourop.com"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    
}


- (IBAction)btnForgot:(id)sender {
    
    // Login invalido
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!!!!"
                                                    message:@"Entre em contato com: contato@fourop.com"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}
@end
