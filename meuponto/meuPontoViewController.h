//
//  meuPontoViewController.h
//  meuponto
//
//  Created by Marcel Medeiros on 11/10/14.
//  Copyright (c) 2014 Fourop. All rights reserved.
//

#import "ViewController.h"

@interface meuPontoViewController : ViewController<UIWebViewDelegate>


@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@property (strong, nonatomic) IBOutlet UIWebView *wv;

@end
