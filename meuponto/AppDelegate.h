//
//  AppDelegate.h
//  meuponto
//
//  Created by Marcel Medeiros on 11/19/14.
//  Copyright (c) 2014 Fourop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

