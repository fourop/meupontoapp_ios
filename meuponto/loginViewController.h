//
//  loginViewController.h
//  meuponto
//
//  Created by Marcel Medeiros on 11/10/14.
//  Copyright (c) 2014 Fourop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface loginViewController : UIViewController


- (IBAction)btnLogin:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
- (IBAction)btnNewAccount:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;

- (IBAction)btnForgot:(id)sender;

@end
