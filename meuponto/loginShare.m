//
//  loginShare.m
//  meuponto
//
//  Created by Marcel Medeiros on 11/10/14.
//  Copyright (c) 2014 Fourop. All rights reserved.
//

#import "loginShare.h"

@implementation loginShare

+(id)sharedInstance{
    
    static dispatch_once_t obj = 0;
    
    __strong static id _sharedObject = nil;
    
    dispatch_once( &obj, ^{
        
        _sharedObject = [[self alloc]init];
        
    });
    
    return _sharedObject;
    
}

@end
