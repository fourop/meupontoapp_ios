//
//  meuPontoViewController.m
//  meuponto
//
//  Created by Marcel Medeiros on 11/10/14.
//  Copyright (c) 2014 Fourop. All rights reserved.
//

#import "meuPontoViewController.h"
#import "loginViewController.h"
#import "loginShare.h"

#define FOUROP_URL_MAIN @"http://master.fourop.com.br/Fourop/"
#define FOUROP_URL_LOGOUT @"index.jsp"

@interface meuPontoViewController ()

@end

@implementation meuPontoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.wv.delegate = self;
    self.wv.translatesAutoresizingMaskIntoConstraints = NO;
    
    loginShare *login = [loginShare sharedInstance];
    
    NSLog(@"%@", login.loginLogin);
    
    [_activity startAnimating];
    NSLog(@" WebView | - | startAnimating ");
    
    NSString *fullURL = login.url;
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:@""];
    [_wv  loadRequest:requestObj];

   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    NSLog(@"webViewDidStartLoad");
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{

    NSLog(@"webViewDidFinishLoad");

     NSLog(@" WebView | - | Stop ");
    
    [_activity stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"didFailLoadWithError");
}

-(BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString;
    NSString *urlLogout = [FOUROP_URL_MAIN stringByAppendingString:FOUROP_URL_LOGOUT];
    
    
    NSLog(@" | shouldStartLoadWithRequest | ");
    NSLog(@" NSSURL %@",urlString);
    
    
    //Check if special link
    if ( [ urlString isEqualToString: urlLogout ] ) {
        
        UIViewController *vc;
        UIStoryboard * mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:vc animated:YES completion:NULL];
        
        return NO;
    }
    
    return YES;
    
}


-(void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    if ( self.presentedViewController)
    {
        [super dismissViewControllerAnimated:flag completion:completion];
    }
}

@end
