//
//  loginShare.h
//  meuponto
//
//  Created by Marcel Medeiros on 11/10/14.
//  Copyright (c) 2014 Fourop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface loginShare : NSObject

@property (strong, nonatomic) NSString *loginLogin;
@property (strong, nonatomic) NSString *url;


+(id)sharedInstance;


@end
