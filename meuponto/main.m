//
//  main.m
//  meuponto
//
//  Created by Marcel Medeiros on 11/19/14.
//  Copyright (c) 2014 Fourop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
